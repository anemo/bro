from django.db import models


class Article(models.Model):
    ano = models.AutoField(primary_key=True, verbose_name='文章序号')
    aname = models.CharField(max_length=255, verbose_name='文章标题')
    aid = models.IntegerField(verbose_name='文章ID')
    acontent = models.TextField(verbose_name='文章内容')
    atype = models.CharField(max_length=25, verbose_name='标签')
    agood = models.IntegerField(verbose_name='赞')
    atime = models.DateTimeField(verbose_name='发表日期',auto_now_add=True)
    author = models.ForeignKey('User', models.DO_NOTHING, db_column='author', blank=True, null=True,
                               verbose_name='文章作者')
    aphoto = models.CharField(default='', max_length=255, verbose_name='图片',db_column='aphoto')

    # def __str__(self):
    #     return self.aname

    class Meta:

        db_table = 'tb_article'


class Comment(models.Model):
    cno = models.AutoField(primary_key=True, verbose_name='评论序号')
    cid = models.IntegerField(verbose_name='评论ID')
    ccontent = models.CharField(max_length=255, verbose_name='评论内容')
    cauthor = models.ForeignKey('User', models.DO_NOTHING, db_column='cauthor', verbose_name='评论用户')
    ctime = models.DateTimeField(auto_now_add=True,verbose_name='评论时间')
    carticle = models.ForeignKey(Article, models.DO_NOTHING, db_column='carticle', verbose_name='所属文章')

    class Meta:

        db_table = 'tb_comment'


class User(models.Model):
    uno = models.AutoField(primary_key=True, verbose_name='用户序号')
    uname = models.CharField(max_length=10, verbose_name='用户名')
    password = models.CharField(max_length=8, verbose_name='密码')
    photo = models.CharField(max_length=255, verbose_name='头像')

    class Meta:

        db_table = 'tb_user'
