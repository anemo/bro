import jwt

from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed

from NbBlog.settings import SECRET_KEY


class LoginAuthentication(BaseAuthentication):
    """登录认证类"""

    def authenticate(self, request):
        token = request.META.get('HTTP_TOKEN')
        if token:
            try:
                result = jwt.decode(token, SECRET_KEY, algorithm='HS256')
                uname = result['data']['uname']
                password = result['data']['password']
                if password:
                    raise AuthenticationFailed('用户名或密码错误无法发布文章')
                uname2 = int(request.data.get('user', '0'))
                if uname != uname2:
                    raise AuthenticationFailed('身份标识失效请重新登录')
                return uname, token
            except Exception:
                pass
        raise AuthenticationFailed('请提供有效的身份认证标识')