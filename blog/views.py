import jwt
from django.shortcuts import render, redirect
from django_filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import api_view
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView, ListAPIView
from rest_framework.response import Response

from NbBlog.settings import SECRET_KEY
from blog.models import Article, User
from blog.serializer import ArticleSerializer


def index(request):

    return redirect('/static/html/home.html')



class ArticleView(ListCreateAPIView,RetrieveUpdateDestroyAPIView):
    '''文章视图'''
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    #筛选和排序
    # filter_backends = ( OrderingFilter)
    # filter_class = ArticleFilterSet
    ordering_fields = ('agood', )


    #重写get方法
    def get(self, request, *args, **kwargs):
        if 'pk' in kwargs:
            cls = RetrieveAPIView
        else:
            cls = ListAPIView
        return cls.get(self, request, *args, **kwargs)


@api_view(('POST', ))
def login(request):
    """登录（获取用户身份令牌）"""
    uname = request.data.get('uname', '')
    password = request.data.get('password', '')

    user = User.objects.filter(uname=uname).first()
    if user:
        payload = {

            'data': {
                'uname': user.uname,
                'password': user.password
            }
        }
        token = jwt.encode(payload, SECRET_KEY, algorithm='HS256').decode()
        data = {
            'code': 10000,
            'token': token,
            'uname': user.uname,
            'password': user.password,
            'photo': user.photo
        }

        if password != user.password:
            data = {'code': 10001, 'message': '用户名或密码错误'}
    else:
        data = {'code': 10002, 'message': '该用户尚未注册'}
    return Response(data)







