from django.urls import path

from blog.views import ArticleView, login

urlpatterns = [


    path('articles/', ArticleView.as_view()),
    path('articles/<int:pk>/', ArticleView.as_view()),
    path('auth/', login)


]
